"use strict"

const carouselOuter = document.getElementsByClassName('carousel-outer');
const carouselInner = document.getElementsByClassName('carousel-inner');
const carouselBtnAreas = document.getElementsByClassName('carousel-dots');

const syncImages = document.getElementsByClassName('sync-images');

const pcMaxWidth = 1020;
let nextSlideSibling;
let autorTimerResumeSec = 4000;

export default function() {

    for (let i = 0; i < carouselInner.length; i++) {
        const carouselImages = carouselInner[i].getElementsByClassName('carousel-item');
        const btnNext = carouselOuter[i].getElementsByClassName('btn-next')[0];
        const btnPrev = carouselOuter[i].getElementsByClassName('btn-prev')[0];
        const carouselImagesLength = carouselImages.length;
        const btnLastChildIndex = carouselImagesLength - 1;
        const slideBtns = carouselBtnAreas[i].getElementsByClassName('dot-each');
        const syncImageEachs = syncImages[i].getElementsByClassName('sync-image-each');
        let slideTimer;
        let onNextTimer;
        let onNextTimerEnd;
        let onNextPrevEnd;
        let onPrevTimer;
        let autoSlider;
        let currentChild;
        let currentChildIndex;
        let currentSlideBtn;
        let currentSyncImage;
        const slideTimerSec = 4000;
        const animationDulation = 1000;
        let winWidth = window.innerWidth;

        let carouselWidth = carouselImages[0].getBoundingClientRect().width;

        let carouselMargin = 2;


        for (let i = 0; i < carouselImages.length; i++) {
            carouselImages[i].addEventListener("mousedown", function (e) {
                // e.stopPropagation();
                return false;
            });
            carouselImages[i].addEventListener("selectstart", function (e) {
                // e.stopPropagation();
                return false;
            });
        }

        //スライドショー用フラッグ
        let isFirst = true;
        let slideMoving = false;

        // サムネイルボタンにトランジションを設定
        for (let i = 0; i < slideBtns.length; i++) {
            slideBtns[i].style.transition = "background-color " + animationDulation / 1000 + 's';
        }

        // 複製前の一番前と一番後ろのカルーセル
        const defaultFirstChild = carouselImages[0];
        const defaultLastChild = carouselImages[carouselImagesLength - 1];

        // 最後の2枚を複製して最初に追加
        carouselInner[i].insertBefore(defaultLastChild.cloneNode(true), defaultFirstChild).classList.add('crone-1');
        carouselInner[i].insertBefore(defaultLastChild.previousElementSibling.cloneNode(true), carouselInner[i].getElementsByClassName('crone-1')[0]).classList.add('crone-2');
        // 最初の2枚を複製して最後に追加
        carouselInner[i].insertBefore(defaultFirstChild.cloneNode(true), defaultLastChild.nextElementSibling || defaultLastChild.nextSibling).classList.add('crone+1');
        carouselInner[i].insertBefore(defaultFirstChild.nextElementSibling.cloneNode(true), carouselInner[i].getElementsByClassName('crone+1')[0].nextElementSibling).classList.add('crone+2');

        // 複製後、複製前の一番前と一番後ろのカルーセルの複製後のindexがどこにあるか
        const defaultFirstChildIndex = 2;
        const defaultLastChildIndex = carouselImagesLength + 1;

        // サイズをレスポンシブする
        function responsiveSlideSize() {
            if (winWidth < 768) {
                carouselInner[i].style.width = (carouselWidth + carouselMargin + carouselMargin) * (3 + 4) + 'px';
            } else {
                carouselInner[i].style.width = (carouselWidth + carouselMargin + carouselMargin) * (3 + 4) + 'px';
            }
            carouselInner[i].style.transform = "translateX(" + (carouselWidth + carouselMargin) * 2 + "px)";
            // if (winWidth > pcMaxWidth) {
            //     carouselInner[i].style.width = pcMaxWidth * (carouselImagesLength + (carouselMargin * 2)) + 'px';
            // } else {
            //     carouselInner[i].style.width = winWidth * (carouselImagesLength + (carouselMargin * 2)) + 'px';
            // }
        }
        responsiveSlideSize();


        // スライドを移動する関数
        function moveSlider(targetIndex, executeTransition) {
            if (executeTransition) {
                carouselInner[i].style.transitionDuration = (animationDulation / 1000) + 's';
            }
            carouselInner[i].style.transform = 'translateX(-' + (carouselWidth + (carouselMargin * 2)) * targetIndex + 'px)';
            carouselInner[i].style.webkitTransform = 'translateX(-' + (carouselWidth + (carouselMargin * 2)) * targetIndex + 'px)';
        }

        // currentクラスを持つ要素とそのインデックスを取得する
        function getCurrentChild() {
            currentChild = carouselInner[i].getElementsByClassName('current')[0];
            currentChildIndex = Array.from(carouselImages).indexOf(currentChild);
        }

        // スライドショーの初期化
        function carouselImagesInit() {
            moveSlider(defaultFirstChildIndex, false);
            carouselImages[2].classList.add('current');
            slideBtns[0].classList.add('current');
            syncImageEachs[0].classList.add('current');
        }
        carouselImagesInit();

        // スライドショーを定義
        const onNext = function () {
            if (!slideMoving) {
                // current classを取得
                getCurrentChild();

                // slideBtnのcurrentClassを取得
                currentSlideBtn = carouselBtnAreas[i].getElementsByClassName('current')[0];
                currentSyncImage = syncImages[i].getElementsByClassName('current')[0];

                if (currentChildIndex === defaultLastChildIndex) {
                    slideMoving = true;

                    // 最後のスライドのときは
                    moveSlider(currentChildIndex + 1, true);

                    currentChild.classList.remove('current');
                    currentSlideBtn.classList.remove('current');
                    currentSyncImage.classList.remove('current');

                    slideBtns[0].classList.add('current');
                    syncImageEachs[0].classList.add('current');

                    onNextTimerEnd = setTimeout(function () {

                        carouselInner[i].style.transitionDuration = 0 + 's';
                        carouselInner[i].style.opacity = '0';

                        moveSlider(defaultFirstChildIndex, false);

                        carouselImages[2].classList.add('current');
                        carouselInner[i].style.opacity = '1';

                        slideMoving = false;
                    }, animationDulation); // animationDulation

                } else {
                    // 最後のスライドのとき以外
                    slideMoving = true;

                    moveSlider(currentChildIndex + 1, true);
                    nextSlideSibling = currentChild.nextElementSibling;

                    // currentクラスを付け替え
                    currentChild.classList.remove('current');
                    currentSlideBtn.classList.remove('current');
                    currentSyncImage.classList.remove('current');
                    nextSlideSibling.classList.add('current');

                    // current classを取得
                    getCurrentChild();
                    // slideBtnにcurrentクラスを付与
                    slideBtns[currentChildIndex - 2].classList.add('current');
                    syncImageEachs[currentChildIndex - 2].classList.add('current');

                    onNextTimer = setTimeout(function () {
                        slideMoving = false;
                    }, animationDulation); //
                }
            }
        }

        const onPrev = function () {
            // clearTimeout(autoSliderFirst);
            // clearInterval(autoSlider);
            autoSlider.pause();
            if (!slideMoving) { //スライドが動いているときは発火しない
                // current classを取得
                getCurrentChild();

                let prevSibling;

                // slideBtnのcurrentClassを取得
                currentSlideBtn = carouselBtnAreas[i].getElementsByClassName('current')[0];
                currentSyncImage = syncImages[i].getElementsByClassName('current')[0];

                if (currentChildIndex === 2) {
                    slideMoving = true;
                    //最初の1枚のとき(clone2枚を除く)
                    moveSlider(currentChildIndex - 1, true);


                    // currentClassを付け替え
                    currentChild.classList.remove('current');
                    currentSlideBtn.classList.remove('current');
                    currentSyncImage.classList.remove('current');


                    slideBtns[btnLastChildIndex].classList.add('current');
                    syncImageEachs[btnLastChildIndex].classList.add('current');

                    onNextPrevEnd = setTimeout(function () {
                        carouselInner[i].style.transitionDuration = 0 + 's';
                        carouselInner[i].style.opacity = '0';

                        moveSlider(defaultLastChildIndex, false);

                        carouselImages[defaultLastChildIndex].classList.add('current');
                        carouselInner[i].style.opacity = '1';

                        slideMoving = false;
                    }, animationDulation);

                } else {
                    //それ以外のとき
                    slideMoving = true;
                    moveSlider(currentChildIndex - 1, true);
                    prevSibling = currentChild.previousElementSibling;

                    // currentクラスを付け替え
                    currentChild.classList.remove('current');
                    currentSlideBtn.classList.remove('current');
                    currentSyncImage.classList.remove('current');
                    prevSibling.classList.add('current');
                    currentSlideBtn.previousElementSibling.classList.add('current');
                    currentSyncImage.previousElementSibling.classList.add('current');

                    onPrevTimer = setTimeout(function () {
                        slideMoving = false;
                    }, animationDulation);
                }
            }
        }

        const onThumbnail = function () {
            // サムネイルボタンを押したときの挙動
            for (let l = 0; l < slideBtns.length; l++) {
                slideBtns[l].addEventListener('click', function (e) {
                    if (!slideMoving) {
                        // オートタイマーを停止
                        // clearTimeout(autoSliderFirst);
                        //clearInterval(autoSlider);
                        autoSlider.pause();

                        // imageのcurrentChildを再取得
                        getCurrentChild();

                        // 最後のボタンをクリックした後に最初のボタンをクリックした場合だけ、右向きに
                        if (currentChildIndex == (carouselImagesLength + 1) && l == 0) {
                            onNext();
                        } else {
                            // 現在image4にいて、image1のサムネイルを押したとき
                            moveSlider(l + 2, true);
                        }

                        // サムネイルボタンにcurrentクラスを付与
                        currentSlideBtn = carouselBtnAreas[i].getElementsByClassName('current')[0];
                        currentSyncImage = syncImages[i].getElementsByClassName('current')[0];
                        if (currentSlideBtn) {
                            currentSlideBtn.classList.remove('current');
                        }
                        if (currentSyncImage) {
                            currentSyncImage.classList.remove('current');
                        }
                        slideBtns[l].classList.add('current');
                        syncImageEachs[l].classList.add('current');

                        //n番目のimageにcurrentクラスを付与しanimatedクラスのz-indexにプラス1した値をcurrentクラスに付与する
                        currentChild.classList.remove('current');
                        carouselImages[l + 2].classList.add('current');

                        // autorTimerResumeSec秒後に再開
                        window.setTimeout(() => {
                            autoSlider.resume();
                        }, autorTimerResumeSec);
                    }
                })
            }
        }
        onThumbnail();

        // autoSlider --------------------------
        function intervalTimer(callback, delay) {
            let callbackStartTime
            let remaining = 0

            this.timerId = null
            this.paused = false

            this.pause = () => {
                this.clear()
                remaining -= Date.now() - callbackStartTime
                this.paused = true
            }
            this.resume = () => {
                window.setTimeout(this.setTimeout.bind(this), remaining)
                this.paused = false
            }
            this.setTimeout = () => {
                this.clear()
                this.timerId = window.setInterval(() => {
                    callbackStartTime = Date.now()
                    callback()
                }, delay)
            }
            this.clear = () => {
                window.clearInterval(this.timerId)
            }

            this.setTimeout()
        }

        // autoPlay タイマーセット
        autoSlider = new intervalTimer(() => {
            onNext();
        }, slideTimerSec);



        // hover 時にはautoPlayを一時停止する
        const onHover = function () {
            if (!slideMoving) { //スライドが動いているときは発火しない
                carouselInner[i].addEventListener('mouseenter', function () {
                    autoSlider.pause();
                });
                carouselInner[i].addEventListener('mouseleave', function () {
                    autoSlider.resume();
                });
            }
        }
        onHover();

        // ------------------ フリック・ドラッグ許可------------------ //
        //タッチ開始
        let startX = null;
        let touchCurrentChildIndex;
        let moveX = 0;

        function touchStart(targetElem, e) {
            startX = e.clientX || e.touches[0].clientX;
            getCurrentChild(); // currentChildとcurrentChildIndexを取得
            touchCurrentChildIndex = currentChildIndex;
            //自動再生タイマーをpause
            autoSlider.pause();
            targetElem.classList.add('dragging');
        }

        function touchMove(targetElem, e) {
            if (startX == null) {
                return;
            }
            e.preventDefault();
            moveX = startX - (e.clientX || e.touches[0].clientX);
            targetElem.style.transform = 'translateX(-' + ((carouselWidth + (carouselMargin * 2)) * touchCurrentChildIndex + moveX) + 'px)';
            targetElem.style.webkitTransform = 'translateX(-' + ((carouselWidth + (carouselMargin * 2)) * touchCurrentChildIndex + moveX) + 'px)';
        }

        function touchEnd(targetElem) {
            startX = null;
            if (moveX >= 0) {
                onNext();
            }
            if (moveX < 0) {
                onPrev();
            }
            moveX = 0;
            targetElem.classList.remove('dragging');
        }


        //ドラッグスタート
        carouselInner[i].addEventListener('mousedown', function (e) {
            if (!slideMoving) { //スライドが動いているときは発火しない
                touchStart(this, e);
            }
        });

        //ドラッグ中
        carouselInner[i].addEventListener('mousemove', function (e) {
            touchMove(this, e);
        });

        //ドラッグ終了
        carouselInner[i].addEventListener('mouseup', function (e) {
            touchEnd(this);
        });

        //タッチスタート
        carouselInner[i].addEventListener('touchstart', function (e) {
            if (!slideMoving) { //スライドが動いているときは発火しない
                touchStart(this, e)
            }
        });

        //タッチ中
        carouselInner[i].addEventListener('touchmove', function (e) {
            touchMove(this, e);
        });

        //タッチ終了
        carouselInner[i].addEventListener('touchend', function (e) {
            touchEnd(this);
        });

        // ------------------ フリック・ドラッグ許可ここまで------------------ //

        if (btnNext != null) {
            btnNext.addEventListener('click', function () {
                // オートタイマーを停止
                // clearTimeout(autoSliderFirst);
                // clearInterval(autoSlider);
                autoSlider.pause();
                onNext();
                window.setTimeout(function () {
                    autoSlider.resume();
                }, autorTimerResumeSec);
            })
        }

        if (btnPrev != null) {
            btnPrev.addEventListener('click', function () {
                // オートタイマーを停止
                // clearTimeout(autoSliderFirst);
                //clearInterval(autoSlider);
                autoSlider.pause();
                onPrev();
                window.setTimeout(function () {
                    autoSlider.resume();
                }, autorTimerResumeSec);
            })
        }


        // リサイズ時初期化
        const resizeInitSlider = function () {
            winWidth = window.innerWidth;
            carouselWidth = carouselImages[0].clientWidth;
            responsiveSlideSize();
            carouselImagesInit();
        }

        // リサイズ時
        let resizeTimer;
        window.addEventListener('resize', function () {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function () {
                resizeInitSlider();
            }, 200)
        })
    }

}