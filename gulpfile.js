// 2019/03/26 改修
// JavascriptのES2015以降のJSのトランスパイル、ファイルの結合・圧縮、ソースマップなどはwebpackにやってもらう。
// javascript関連の詳しい設定はwebpack.config.jsを編集する
// bootstrapを追加（2019/04/20）。bootstarapのプラグインを使用するのであればjQueryが必須となる。
// browsersyncでサーバーを立ち上げる仕様に変更(2019/5/28)
// autoprefixerのbrowsersオプションは非推奨のため削除。代わりにpackage.jsonにbrowserslistを追加（2019/09/10）
// webpack.configをgulpfile.jsに含めた（2019/10/02）
// cssのメディアクエリーをまとめる「gulp-group-css-media-queries」を追加（2021/01）

// taskは非推奨なので書き換える(2021/05/28) 参考　https://note.com/kei_01011/n/n34220e142583

const { src, dest, watch, series, parallel, task } = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const plumber = require('gulp-plumber');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const webpackStream = require("webpack-stream");
const webpack = require("webpack");
const gcmq = require('gulp-group-css-media-queries');

// Image compression
const imagemin = require('gulp-imagemin');
const imageminGifsicle = require('imagemin-gifsicle');
const imageminPngquant = require('imagemin-pngquant');
const imageminMozjpeg = require("imagemin-mozjpeg");
const imageminSvgo = require("imagemin-svgo");

//file path
const ROOT_PATH = './';
const SCSS_PATH = ROOT_PATH + '_assets/scss/**/*.scss';
const SCRIPTS_PATH = ROOT_PATH + '_assets/js/**/*.js';
const IMAGES_PATH = ROOT_PATH + '_assets/images/**/*.{png,jpeg,jpg,svg,gif}';
const DIST_PATH = ROOT_PATH + 'dist/';

const webpackConfig = {
    // production:公開用
    // development:開発用（ソースマップ有効)
    mode: "development",

    // 複数エントリポイントにも対応
    entry: { project: './_assets/js/project.js' }, //main以外の名前を指定
    // 出力ファイル
    output: {
        filename: "[name].min.js",
        // chunkLoading: false,
        // wasmLoading: false,
    },
    target: 'web',
    // target: ['browserslist'], // =>動くけど、jsファイルが生成されない
    module: {
        rules: [
            {
                // 拡張子 .js の場合
                test: /\.js$/,
                use: [
                    {
                        // Babel を利用する
                        loader: "babel-loader",
                        // Babel のオプションを指定する
                        options: {
                            presets: [
                                // プリセットを指定することで、ES2018 を ES5 に変換
                                "@babel/preset-env"
                            ]
                        }
                    }
                ]
            }
        ],
    },

    // optimization.splitChunks オプションを指定することで、外部パッケージのコードのみ別ファイルに出力できる。
    // ブラウザキャッシュを有効活用できるため、複数ページあるサイトを作成する場合は使用する
    // , optimization: {
    //     splitChunks: {
    //         name: 'vendor',
    //         chunks: 'initial' // initial,async,all
    //     }
    // }

    // バンドルしたファイル（もしくはvender.min.js）にjQueryファイルをインクルードしない場合はexternalsオプションを使用する
    // 別途HTMLからjQueryをロード。jQueryを使用するファイルに「import $ from 'jquery';」を記述する。
    // , externals: [
    //     {
    //         jquery: 'jQuery'
    //     }
    // ],
    plugins: [
        // jQueryが使えるようにする。個々のファイルでの「import $ from 'jquery';」の指定は不要。
        // externalsを指定しなかった場合はバンドルしたファイル（もしくはvender.min.js）にjQueryファイルがインクルードされる
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        })
    ]
}

// Styles for SCSS
const styles = () => {
    return src(SCSS_PATH)
        .pipe(plumber(function (err) {
            console.log('Style task error');
            console.log(err);
            this.emit('end');
        }))
        .pipe(sourcemaps.init())
        .pipe(autoprefixer())
        // いったんsassをCSSにコンパイルする
        .pipe(sass({
            outputStyle: 'expanded'
        }).on('error', sass.logError))
        // メディアクエリーの記述をまとめる
        .pipe(gcmq())
        // 'expanded','nested','compact',or'compressed'
        // .pipe(sass({
        //     outputStyle: 'compressed'
        // }).on('error', sass.logError))
        // .pipe(sourcemaps.write()) //納品時は外す
        .pipe(dest(DIST_PATH + 'css'));
}

// Scripts
const scripts = () => {
    return webpackStream(webpackConfig,webpack)
        .pipe(plumber(function (err) {
            console.log('Scripts task error');
            console.log(err);
            this.emit('end');
        }))
        .pipe(dest(DIST_PATH + 'js/'))
}

// images
const images = () => {
    return src(IMAGES_PATH)
        .pipe(imagemin(
            [
                imagemin.gifsicle(),
                imageminMozjpeg({
                    quality:60
                }),
                imageminSvgo(),
                imageminPngquant({
                    quality: [0.3, 0.6], // Min and max are numbers in range 0 (worst) to 1 (perfect), similar to JPEG.
                    speed: 1 // The lowest speed of optimization with the highest quality(1 (brute-force) to 11 (fastest))
                })
            ]
        ))
        .pipe(dest(DIST_PATH + 'images/'));
}

// browser-syncでサーバーを立ち上げる
const buildServer = (done) => {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    done();
    console.log('Server was launched');
}

// ブラウザのリロード
const browserReload = (done) => {
    browserSync.reload();
    done();
    console.log('Browser reload completed');
}

//watch
const watchFiles = (done) => {
    watch(SCSS_PATH, series(styles, browserReload) );
    watch(SCRIPTS_PATH, series(scripts, browserReload));
    watch(IMAGES_PATH, series(images)); // イメージのみbrowserReloadを行わない
    watch('./**/*.html', series(browserReload));
    done();
    console.log(('gulp watch started'));
}

exports.default = series(series(styles, scripts,images),parallel(buildServer, watchFiles));